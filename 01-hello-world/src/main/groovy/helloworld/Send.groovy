package helloworld

import com.rabbitmq.client.Channel
import com.rabbitmq.client.Connection
import com.rabbitmq.client.ConnectionFactory

/**
 * Created by Andrew Canby on 31/08/2016.
 */
class Send {

    private static final String QUEUE_NAME = "hello"

    public static void main(String[] args) {
        ConnectionFactory factory = new ConnectionFactory()
        factory.setHost("localhost")
        Connection connection = factory.newConnection()
        Channel channel = connection.createChannel()

        channel.queueDeclare(QUEUE_NAME, false, false, false, null)
        String message = "Hello World!"
        10.times {
            channel.basicPublish("", QUEUE_NAME, null, message.getBytes())
        }
        println " [x] Sent '$message'"

        channel.close()
        connection.close()
    }
}
