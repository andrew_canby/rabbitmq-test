package topics

import com.rabbitmq.client.Channel
import com.rabbitmq.client.Connection
import com.rabbitmq.client.ConnectionFactory

/**
 * Created by Andrew Canby on 1/09/2016.
 */
class EmitLogTopic {

    private static final String EXCHANGE_NAME = "topic_logs"

    private static final Collection<People> people = People.values()
    private static final Collection<LogLevel> levels = LogLevel.values()

    public static void main(String[] args) {
        ConnectionFactory factory = new ConnectionFactory()
        factory.setHost("localhost")
        Connection connection = factory.newConnection()
        Channel channel = connection.createChannel()

        channel.exchangeDeclare(EXCHANGE_NAME, "topic")

        for (String message : args) {
            String routingKey = getRouting()
            channel.basicPublish(EXCHANGE_NAME, routingKey, null, message.getBytes())
            println " [x] Sent '$routingKey': '$message'"
        }

        connection.close()
    }

    static String getRouting() {
        int person = new Random().nextInt(people.size())
        int level = new Random().nextInt(levels.size())

        return people.getAt(person).name() + "." + levels.getAt(level).name()
    }
}
