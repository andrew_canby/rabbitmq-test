package topics

/**
 * Created by Andrew Canby on 1/09/2016.
 */
enum People {
    ANDREW,
    MATT,
    CHRIS,
    PETER
}