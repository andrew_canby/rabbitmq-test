package topics

import com.rabbitmq.client.AMQP
import com.rabbitmq.client.Channel
import com.rabbitmq.client.Connection
import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.Consumer
import com.rabbitmq.client.DefaultConsumer
import com.rabbitmq.client.Envelope

/**
 * Created by Andrew Canby on 1/09/2016.
 */
class ReceiveLogsTopic {

    private static final String EXCHANGE_NAME = "topic_logs"

    private static final Collection<People> people = People.values()
    private static final Collection<LogLevel> levels = LogLevel.values()

    public static void main(String[] args) {
        ConnectionFactory factory = new ConnectionFactory()
        factory.setHost("localhost")
        Connection connection = factory.newConnection()
        Channel channel = connection.createChannel()

        channel.exchangeDeclare(EXCHANGE_NAME, "topic")
        String queueName = channel.queueDeclare().getQueue()

        for (int i=0;i<3;i++) {
            String bindingKey = getBindingKey()
            channel.queueBind(queueName, EXCHANGE_NAME, bindingKey)
            println " [x] Binding to '$bindingKey'"
        }

        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8")
                println " [x] Received '${envelope.getRoutingKey()}': '$message'"
            }
        }

        channel.basicConsume(queueName, true, consumer)
    }

    static String getBindingKey() {
        String person = randomise(people)
        String level = randomise(levels)

        return "$person.$level"
    }

    static String randomise(Collection collection) {
        int index = new Random().nextInt(collection.size() + 2)

        if (index < collection.size()) {
            return collection.getAt(index)
        } else if (index == collection.size()) {
            return "#"
        } else {
            return "*"
        }
    }
}
