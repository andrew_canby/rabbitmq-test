package topics
/**
 * Created by Andrew Canby on 1/09/2016.
 */
enum LogLevel {

    TRACE(0),
    DEBUG(1),
    INFO(2),
    WARN(3),
    ERROR(4),
    FATAL(5)

    public LogLevel(int level) {
        this.level = level
    }

    int level
}