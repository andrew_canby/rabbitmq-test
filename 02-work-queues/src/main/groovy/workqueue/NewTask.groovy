package workqueue

import com.rabbitmq.client.Channel
import com.rabbitmq.client.Connection
import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.MessageProperties

/**
 * Created by Andrew Canby on 31/08/2016.
 */
class NewTask {

    private static final String QUEUE_NAME = 'task_queue'

    public static void main(String[] args) {
        ConnectionFactory factory = new ConnectionFactory()
        factory.setHost("localhost")
        Connection connection = factory.newConnection()
        Channel channel = connection.createChannel()

        channel.queueDeclare(QUEUE_NAME, true, false, false, null)

        10.times {
            String message = getMessage(args) + it.toString()
            channel.basicPublish("", QUEUE_NAME, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes())
            println " [x] Sent '$message'"
        }

        channel.close()
        connection.close()
    }

    static String getMessage(String[] strings) {
        if (strings.length < 1) {
            return "Hello World!"
        }

        return strings.join(".")
    }
}
