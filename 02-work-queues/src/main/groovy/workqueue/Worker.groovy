package workqueue

import com.rabbitmq.client.AMQP
import com.rabbitmq.client.Channel
import com.rabbitmq.client.Connection
import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.Consumer
import com.rabbitmq.client.DefaultConsumer
import com.rabbitmq.client.Envelope

import java.time.LocalTime

/**
 * Created by Andrew Canby on 31/08/2016.
 */
class Worker {

    private static final String QUEUE_NAME = 'task_queue'

    public static void main(String[] args) {
        ConnectionFactory factory = new ConnectionFactory()
        factory.setHost("localhost")
        Connection connection = factory.newConnection()
        Channel channel = connection.createChannel()

        channel.queueDeclare(QUEUE_NAME, true, false, false, null)

        println " [*] Waiting for messages. To exit press CTRL+C"

        channel.basicQos(2)

        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8")
                LocalTime start = LocalTime.now()
                println " [x] Received '$message' at $start"

                try {
                    doWork(message)
                } finally {
                    LocalTime elapsed = LocalTime.now().minusNanos(start.toNanoOfDay())
                    println " [x] Finshed processing at ${LocalTime.now()}. Took $elapsed"
                    channel.basicAck(envelope.getDeliveryTag(), false)
                }
            }
        }

        channel.basicConsume(QUEUE_NAME, false, consumer)
    }

    private static void doWork(String task) throws InterruptedException {
        new Integer(task.count('.')).times {
            Thread.sleep(1000)
        }
    }
}
