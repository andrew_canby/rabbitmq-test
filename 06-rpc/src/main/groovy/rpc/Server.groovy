package rpc

import com.rabbitmq.client.*

/**
 * Created by Andrew Canby on 1/09/2016.
 */
class Server {

    private static final String QUEUE_NAME = 'fib_queue'

    public static void main(String[] args) {
        ConnectionFactory factory = new ConnectionFactory()
        factory.setHost("localhost")
        Connection connection = factory.newConnection()
        Channel channel = connection.createChannel()

        channel.queueDeclare(QUEUE_NAME, false, false, false, null)
        channel.basicQos(1)

        QueueingConsumer consumer = new QueueingConsumer(channel)
        channel.basicConsume(QUEUE_NAME, false, consumer)

        while (true) {
            QueueingConsumer.Delivery delivery = consumer.nextDelivery()
            BasicProperties properties = delivery.getProperties()
            BasicProperties replyProperties = new AMQP.BasicProperties()
                    .builder()
                    .correlationId(properties.getCorrelationId())
                    .build()

            String message = new String(delivery.getBody())
            BigInteger n = new BigInteger(message)

            println " [.] fib($n)"
            String response = "" + fib(n)

            channel.basicPublish("", properties.getReplyTo(), replyProperties, response.getBytes())
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false)
        }
    }

    static BigInteger fib(BigInteger i) {
        if (cache.containsKey(i)) {
            return cache.get(i)
        }

        if (i == BigInteger.ZERO) {
            return BigInteger.ZERO
        }
        if (i == BigInteger.ONE) {
            return BigInteger.ONE
        }

        BigInteger left
        left = fib(i.subtract(BigInteger.ONE))

        BigInteger right
        right = fib(i.subtract(BigInteger.valueOf(2)))

        BigInteger result = left + right
        cache.put(i, result)

        return result
    }

    static Map<BigInteger, BigInteger> cache = new TreeMap<>()
}
