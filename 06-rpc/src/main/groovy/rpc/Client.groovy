package rpc

import com.rabbitmq.client.*

/**
 * Created by Andrew Canby on 1/09/2016.
 */
class Client {
    public static final String requestQueueName = "fib_queue"

    QueueingConsumer consumer
    String replyToQueue
    Channel channel
    Connection connection

    public Client() {
        ConnectionFactory factory = new ConnectionFactory()
        factory.setHost("localhost")
        connection = factory.newConnection()
        channel = connection.createChannel()

        replyToQueue = channel.queueDeclare().getQueue()
        consumer = new QueueingConsumer(channel)
        channel.basicConsume(replyToQueue, true, consumer)
    }

    public String call(String message) {
        String correlationId = UUID.randomUUID().toString()

        AMQP.BasicProperties properties = new AMQP.BasicProperties()
                .builder()
                .correlationId(correlationId)
                .replyTo(replyToQueue)
                .build()

        channel.basicPublish("", requestQueueName, properties, message.getBytes())

        while (true) {
            QueueingConsumer.Delivery delivery = consumer.nextDelivery()
            if (delivery.getProperties().getCorrelationId() == correlationId) {
                return new String(delivery.getBody())
            }
        }
    }

    public void close() {
        connection.close()
    }

    public static void main(String[] args) {
        Client client = new Client()

        294.times {
            println " [x] Requesting fib($it)"
            String response = client.call("$it")
            println " [x] Got '$response'"
        }

        client.close()
    }
}
