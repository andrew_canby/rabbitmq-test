package pubsub

import com.rabbitmq.client.Channel
import com.rabbitmq.client.Connection
import com.rabbitmq.client.ConnectionFactory

/**
 * Created by Andrew Canby on 1/09/2016.
 */
class EmitLog {

    private static final String EXCHANGE_NAME = "logs"

    public static void main(String[] args) {

        ConnectionFactory factory = new ConnectionFactory()
        factory.setHost("localhost")
        Connection connection = factory.newConnection()
        Channel channel = connection.createChannel()

        channel.exchangeDeclare(EXCHANGE_NAME, "fanout")

        String message = args.join(".")

        channel.basicPublish(EXCHANGE_NAME, "", null, message.getBytes())
        println " [x] Sent '$message'"

        channel.close()
        connection.close()
    }
}
