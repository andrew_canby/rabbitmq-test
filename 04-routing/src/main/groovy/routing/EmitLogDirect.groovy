package routing

import com.rabbitmq.client.Channel
import com.rabbitmq.client.Connection
import com.rabbitmq.client.ConnectionFactory

/**
 * Created by Andrew Canby on 1/09/2016.
 */
class EmitLogDirect {

    private static final String EXCHANGE_NAME = "direct"
    private static final Collection<LogLevel> logLevels = LogLevel.values()

    public static void main(String[] args) {
        ConnectionFactory factory = new ConnectionFactory()
        factory.setHost("localhost")
        Connection connection = factory.newConnection()
        Channel channel = connection.createChannel()

        channel.exchangeDeclare(EXCHANGE_NAME, "direct")

        for (String message : args) {
            String severity = getSeverity()
            channel.basicPublish(EXCHANGE_NAME, severity, null, message.getBytes())
            println " [x] Sent '$message' at log level '$severity'"
        }

        channel.close()
        connection.close()
    }

    static String getSeverity() {
        int level = new Random().nextInt(6)
        return logLevels.getAt(level).name()
    }
}
