package routing

import com.rabbitmq.client.AMQP
import com.rabbitmq.client.Channel
import com.rabbitmq.client.Connection
import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.Consumer
import com.rabbitmq.client.DefaultConsumer
import com.rabbitmq.client.Envelope

/**
 * Created by Andrew Canby on 1/09/2016.
 */
class ReceiveLogsDirect {
    private static final String EXCHANGE_NAME = "direct"
    private static final Collection<LogLevel> logLevels = LogLevel.values()

    public static void main(String[] args) {
        ConnectionFactory factory = new ConnectionFactory()
        factory.setHost("localhost")
        Connection connection = factory.newConnection()
        Channel channel = connection.createChannel()

        channel.exchangeDeclare(EXCHANGE_NAME, "direct")
        String queueName = channel.queueDeclare().getQueue()

        int logLevel = getMinLogLevel()

        for (int i=logLevel;i<logLevels.size();i++) {
            // subscribe to all >= the min level
            String logLevelName = logLevels.getAt(i).name()
            channel.queueBind(queueName, EXCHANGE_NAME, logLevelName)
            println " [x] Subscribed to logs at level $logLevelName"
        }

        println " [x] Waiting for messages. To exit press CTRL+C"

        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8")
                println " [x] Received message '$message' for log level '${envelope.getRoutingKey()}'"
            }
        }

        channel.basicConsume(queueName, true, consumer)
    }

    static int getMinLogLevel() {
        new Random().nextInt(6)
    }

}